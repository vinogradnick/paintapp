﻿using PaintApp.Tools;
using AForge;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Imaging.Filters;

namespace PaintApp
{
    public partial class MainView : Form
    {
        #region Properties
        private int _childFormNumber = 0;
        public static Color Color;
        public static CoreTool Tool;
        public static int ToolSize;
        public static int scale =1;

        public static PaintView ChildView;
        public static string ToolName;
        private Image NormalImage;
        
        #endregion
        public MainView()
        {
            InitializeComponent();
            Color = Color.Black;
            ToolSize = 3;
        }
        #region FormTools
        private void ShowNewForm(object sender, EventArgs e)
        {
            PaintView childForm = new PaintView {MdiParent = this, Text = "Окно " + _childFormNumber++};
            childForm.Show();
            ChildView = childForm;
            Tool = new PenTool(ChildView);
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                Filter = @"Файлы изображений (*.bmp)|*.bmp|Все файлы (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                PaintView view = new PaintView(new Bitmap(Path.GetFullPath(openFileDialog.FileName)))
                {
                    MdiParent = this, Text = $@"Окно {openFileDialog.FileName} |" + _childFormNumber++
                };
                view.Show();
                ChildView = view;
                Tool = new PenTool(ChildView);

            }

        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                Filter = @"Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*"
            };
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;
                ChildView.pictureBox.Image.Save(fileName+".bmp");
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e) => this.Close();
        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e) => LayoutMdi(MdiLayout.Cascade);

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e) => LayoutMdi(MdiLayout.TileVertical);

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e) => LayoutMdi(MdiLayout.TileHorizontal);

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e) => LayoutMdi(MdiLayout.ArrangeIcons);

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
                childForm.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            colorDialog1 = new ColorDialog();
            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.BackColor = colorDialog1.Color;
                Color = colorDialog1.Color;
            }
        }
        private void trackBar1_Scroll(object sender, EventArgs e) => ToolSize = trackBar1.Value;

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (Tool != null)
            {
                ToolName = "pencil";
                Tool.DisposeTool();
                Tool = new PenTool(ChildView);
            }
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (Tool != null)
            {
                ToolName = "ellipse";
                Tool.DisposeTool();
                Tool = new EllipseTool(ChildView);
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (Tool != null)
            {
                ToolName = "eraser";
                Tool.DisposeTool();
                Tool = new EraserTool(ChildView);
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (Tool != null)
            {
                ToolName = "star";
                Tool.DisposeTool();
                Tool = new StarTool(ChildView);
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (scale < 4)
                scale++;
            ZoomImage();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            UnZoomImage();
            scale--;
        }

        private void размытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Blur blur = new Blur();
            ChildView.pictureBox.Image = blur.Apply((Bitmap)ChildView.pictureBox.Image);
        }

        private void повернутьНалевоToolStripMenuItem_Click(object sender, EventArgs e) => ChildView.RotateImage(RotateFlipType.Rotate90FlipNone);
        private void повернутьНаправоToolStripMenuItem_Click(object sender, EventArgs e) => ChildView.RotateRight();

        private void резкостьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Sharpen sharpen = new Sharpen();
            ChildView.pictureBox.Image = sharpen.Apply((Bitmap)ChildView.pictureBox.Image);
        }

        private void ZoomImage()
        {
            try
            {
                Bitmap image = new Bitmap(ChildView.pictureBox.Image, ChildView.pictureBox.Width * scale, ChildView.pictureBox.Height * scale);
                Graphics g = Graphics.FromImage(image);
                g.InterpolationMode = InterpolationMode.Default;
                ChildView.pictureBox.Image = image;
                ChildView.ScrollBarActivate();

            }
            catch (Exception)
            {
                MessageBox.Show("Невозможно выполнить масштабирование");
            }
           
        }
        private void UnZoomImage()
        {
            try
            {
               
                Bitmap image = new Bitmap(ChildView.pictureBox.Image, ChildView.pictureBox.Width / scale, ChildView.pictureBox.Height / scale);
                Graphics g = Graphics.FromImage(image);
                g.InterpolationMode = InterpolationMode.Default;
                ChildView.pictureBox.Image = image;
                ChildView.ScrollBarActivate();

            }
            catch
            {
                MessageBox.Show("Невозможно выполнить масштабирование");
            }

        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            ToolName="line";
            Tool.DisposeTool();
            Tool = new LineTool(ChildView);
        }

        private void отразитьПоГоризонталиToolStripMenuItem_Click(object sender, EventArgs e) => ChildView.RotateImage(RotateFlipType.RotateNoneFlipX);
        private void отразитьПоВертикалиToolStripMenuItem_Click(object sender, EventArgs e) => ChildView.RotateImage(RotateFlipType.RotateNoneFlipY);
        #endregion


        private void эффектToolStripMenuItem_Click(object sender, EventArgs e) => NegativeEffect();

        private void NegativeEffect()
        {
            Bitmap bitmap = new Bitmap(ChildView.pictureBox.Image);
            int dispX = 1, dispY = 1;
            for (var i = 0; i < bitmap.Height - 2; ++i)
            for (var j = 0; j < bitmap.Width - 2; ++j)
            {
                Color pixel1 = new Color(), pixel2 = new Color();
                pixel1 = bitmap.GetPixel(j, i);
                pixel2 = bitmap.GetPixel(j + dispX, i + dispY);
                var red = Math.Min(Math.Abs(pixel1.R - pixel2.R) + 128, 255);
                var green = Math.Min(Math.Abs(pixel1.G - pixel2.G) + 128, 255);
                var blue = Math.Min(Math.Abs(pixel1.B - pixel2.B) + 128, 255);
                bitmap.SetPixel(j, i, Color.FromArgb(red, green, blue));
            }

            ChildView.pictureBox.Image = bitmap;

        }

        private void GreenEffect()
        {
            Bitmap bitmap =new Bitmap(ChildView.pictureBox.Image);
            int DX = 1, DY = 1;
            for (var i = DX; i < bitmap.Height - DY - 1; ++i)
            for (var j = DY; j < bitmap.Width - DY - 1; ++j)
            {
                var red = (int)(bitmap.GetPixel(j, i).R + 0.5 * bitmap.GetPixel(j, i).R
                                - bitmap.GetPixel(j - DX, i - DY).R);
                var green = (int)(bitmap.GetPixel(j, i).G + 0.7 * bitmap.GetPixel(j, i).G
                                  - bitmap.GetPixel(j - DX, i - DY).G);
                var blue = (int)(bitmap.GetPixel(j, i).B + 0.5 * bitmap.GetPixel(j, i).B
                                 - bitmap.GetPixel(j - DX, i - DY).B);
                red = Math.Min(Math.Max(red, 0), 255);
                green = Math.Min(Math.Max(green, 0), 255);
                blue = Math.Min(Math.Max(blue, 0), 255);
                bitmap.SetPixel(j, i, Color.FromArgb(red, green, blue));
            }

            ChildView.pictureBox.Image = bitmap;
        }

        private void эффектЗеленогоToolStripMenuItem_Click(object sender, EventArgs e) => GreenEffect();
    }
}
