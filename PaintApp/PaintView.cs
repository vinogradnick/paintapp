﻿using PaintApp.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp
{
    public partial class PaintView : Form
    {
        /// <summary>
        /// GDI square of current window
        /// </summary>
        public Graphics Graphics { get; set; }
        /// <summary>
        /// Есть ли возможность расширять окно
        /// </summary>
        private bool CanResize => !(MainView.scale > 1);

        private Panel _panel;
        /// <summary>
        /// Конструктор без параметров когда нет картинки
        /// </summary>
        public PaintView()
        {
            InitializeComponent();
            Bitmap image = new Bitmap(this.Width, this.Height);
            Graphics = Graphics.FromImage(image);
            this.pictureBox.Image = image;
            this.Width = image.Width;
            this.Height = image.Height;
        }
        /// <summary>
        /// Конструктор с параметрами для ситуации открытия картинки
        /// </summary>
        /// <param name="image"></param>
        public PaintView(Bitmap image)
        {
            InitializeComponent();
            Graphics = Graphics.FromImage(image);
            this.pictureBox.Image = image;
            Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
        }
        /// <summary>
        /// Изменение размера окна при перетаскивании
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PaintView_Resize(object sender, EventArgs e)
        {
            if (CanResize)
            {
                if (_panel != null)
                {
                    _panel.Width = this.Width;
                    _panel.Height = this.Height;
                    this.AutoScroll = false;
                }
                this.pictureBox.Width = this.Width;
                this.pictureBox.Height = this.Height;
                ResizeWindow();
            }
           
        }
        /// <summary>
        /// Масштабирование окна под заданые размеры
        /// </summary>
        private void ResizeWindow()
        {
            Bitmap image = (Bitmap)this.pictureBox.Image;
            try
            {
                Bitmap bitmap = new Bitmap(this.pictureBox.Width, this.pictureBox.Height);
                int x = Math.Min(pictureBox.Width, image.Width);
                int y = Math.Min(pictureBox.Width, image.Height);
                for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    bitmap.SetPixel(i, j, image.GetPixel(i, j));
                pictureBox.Image = bitmap;
            }
            catch (Exception)
            {
                // ignored
            }
        }
        /// <summary>
        /// Изменение размера при повороте картинки
        /// </summary>
        /// <param name="image"></param>
        public void ChildResize()
        {
            this.Height = this.pictureBox.Height;
            this.Width = this.pictureBox.Width;
        }

        private void PaintView_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Активация инструмента при фокусировке на окне
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Activate_Window(object sender, EventArgs e)
        {
           
            MainView.ChildView = this;
            if (MainView.Tool != null)
              MainView.Tool.DisposeTool();

            switch (MainView.ToolName)
            {
                case "pencil":
                    MainView.Tool = new PenTool(this);
                        break;
                case "line":
                    MainView.Tool = new LineTool(this);
                    break;
                case "ellipse":
                    MainView.Tool = new EllipseTool(this);
                    break;
                case "eraser":
                    MainView.Tool = new EraserTool(this);
                    break;
                case "star":
                    MainView.Tool = new StarTool(this);
                    break;
            }
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {

        }
        /// <summary>
        /// Поворот картинки
        /// </summary>
        /// <param name="type">тип поворота</param>
        public void RotateImage(RotateFlipType type)
        {
            Image image = this.pictureBox.Image;
            image.RotateFlip(type);
            this.pictureBox.Image = image;
            this.ChildResize();
        }
        /// <summary>
        /// Поворот направо
        /// </summary>
        public void RotateRight()
        {
            Image image = this.pictureBox.Image;
            image.RotateFlip(RotateFlipType.Rotate90FlipNone);
            image.RotateFlip(RotateFlipType.RotateNoneFlipX);
            this.pictureBox.Image = image;
            ChildResize();
        }
        private void PaintView_Scroll(object sender, ScrollEventArgs e)
        {

        }

        /// <summary>
        /// Активация скрола при масштабировании картинки
        /// </summary>
        public void ScrollBarActivate()
        {
            if (_panel != null)
            {
                if (!CanResize)
                    this.AutoScroll = true;
                _panel.Size = this.pictureBox.Image.Size;
            }
            if (_panel == null)
            {
                this.AutoScroll = true;
                _panel = new Panel
                {
                    AutoScroll = true, Size = this.pictureBox.Image.Size, Name = "panel1", TabIndex = 8
                };
                _panel.Controls.Add(pictureBox);

                this.Controls.Add(_panel);
            }
        }
    }
}
