﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    /// <summary>
    /// 
    /// This is tool includes all other tool and he is parent tool
    /// </summary>
    public abstract class CoreTool
    {
        /// <summary>
        /// status of 
        /// </summary>
        protected bool MousePressed;
        protected PaintView View;

        protected CoreTool(PaintView view)
        {
            view.pictureBox.MouseUp += new MouseEventHandler(this.MouseUp);
            view.pictureBox.MouseMove += new MouseEventHandler(this.MouseMove);
            view.pictureBox.MouseDown += new MouseEventHandler(this.MouseDown);
            this.View = view;
        }
        public virtual void DisposeTool()
        {
            View.pictureBox.MouseMove -= new MouseEventHandler(this.MouseMove);
            View.pictureBox.MouseDown -= new MouseEventHandler(this.MouseDown);
            View.pictureBox.MouseUp -= new MouseEventHandler(this.MouseUp);
        }
        public abstract void MouseMove(object sender, MouseEventArgs args);
        public abstract void MouseUp(object sender, MouseEventArgs args);
        public abstract void MouseDown(object sender, MouseEventArgs args);
        
    }
}
