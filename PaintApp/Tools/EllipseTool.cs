﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    class EllipseTool : CoreTool
    {
        Graphics tempGraphics;
        Point beg;
        Point cur;
        Pen pen;
        Image image;
        public EllipseTool(PaintView view) : base(view)
        {
        }

        public override void MouseDown(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left)
            {
                MousePressed = true;
                beg = args.Location;
                image = View.pictureBox.Image;
                // tempGraphics = Graphics.FromImage(view.pictureBox.Image);


            }
        }

        public override void MouseMove(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                cur = args.Location;
                this.Paint();
            }
        }
        public void Paint()
        {
            try
            {
                pen = new Pen(MainView.Color, MainView.ToolSize);
                View.pictureBox.Image = (Image)image.Clone();
                
                tempGraphics = Graphics.FromImage(View.pictureBox.Image);
                tempGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                tempGraphics.DrawEllipse(pen, GetRectangleFromPoints(beg, cur));
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public override void MouseUp(object sender, MouseEventArgs args)
        {
            try
            {
                if (MousePressed)
                {
                    MousePressed = false;

                    //tempGraphics.DrawLine(pen, beg, args.Location);
                    View.pictureBox.Invalidate();
                    tempGraphics.DrawEllipse(pen, GetRectangleFromPoints(beg, cur));
                    pen.Dispose();
                    tempGraphics.Dispose();
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }
        protected Rectangle GetRectangleFromPoints(Point p1, Point p2)
        {
            Point oPoint;
            Rectangle rect;

            if ((p2.X > p1.X) && (p2.Y > p1.Y))
            {
                rect = new Rectangle(p1, new Size(p2.X - p1.X, p2.Y - p1.Y));
            }
            else if ((p2.X < p1.X) && (p2.Y < p1.Y))
            {
                rect = new Rectangle(p2, new Size(p1.X - p2.X, p1.Y - p2.Y));
            }
            else if ((p2.X > p1.X) && (p2.Y < p1.Y))
            {
                oPoint = new Point(p1.X, p2.Y);
                rect = new Rectangle(oPoint, new Size(p2.X - p1.X, p1.Y - oPoint.Y));
            }
            else
            {
                oPoint = new Point(p2.X, p1.Y);
                rect = new Rectangle(oPoint, new Size(p1.X - p2.X, p2.Y - p1.Y));
            }
            return rect;
        }
    }
}
