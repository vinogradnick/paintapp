﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    class EraserTool : PenTool
    {
        public EraserTool(PaintView view) : base(view)
        {
        }
        public override void Paint()
        {

            Pen pen = new Pen(Color.White, MainView.ToolSize);

            Graphics graphics = Graphics.FromImage(View.pictureBox.Image);
     
            graphics.DrawLine(pen, Beg, Curr);
            View.pictureBox.Invalidate();
        }
    }
}
