﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    class LineTool :CoreTool
    {
        Graphics _tempGraphics;
        Point beg;
        Point cur;
        Pen pen;
        Rectangle rect;
        Image image;
        public LineTool(PaintView view) : base(view)
        {
        }


        public override void MouseDown(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left)
            {
                MousePressed = true;
                beg = args.Location;
                image = View.pictureBox.Image;
               // tempGraphics = Graphics.FromImage(view.pictureBox.Image);
            }
        }

        public override void MouseMove(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                cur = args.Location;
                this.Paint();
            }
        }
        public void Paint()
        {
            pen = new Pen(MainView.Color, MainView.ToolSize);
            View.pictureBox.Image = (Image)image.Clone();
            _tempGraphics = Graphics.FromImage(View.pictureBox.Image);
            _tempGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            _tempGraphics.DrawLine(pen, beg, cur);

        }

        public override void MouseUp(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                try
                {
                    MousePressed = false;

                    //tempGraphics.DrawLine(pen, beg, args.Location);
                    View.pictureBox.Invalidate();
                    _tempGraphics.DrawLine(pen, beg, args.Location);
                    pen.Dispose();
                    _tempGraphics.Dispose();
                }
                catch (Exception)
                {

                }
               
            }
        }
    }
}
