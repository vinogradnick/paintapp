﻿using System.Drawing;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    class PenTool : CoreTool
    {
        protected Point Curr;//current position of mouse cursor
        protected Point Beg;//begin position of mouse cursor

        public PenTool(PaintView view) : base(view)
        {
        }

        /// <summary>
        /// This is event work when user is pressed mouse button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public  override  void MouseDown(object sender, MouseEventArgs args)
        {
            MousePressed = true;
            Curr = args.Location;// current position of mouse
        }

        public  override void MouseMove(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                Beg = Curr;
                Curr = args.Location;
                Paint();

            }
        }
        public virtual void Paint()
        {
            Pen pen = new Pen(MainView.Color,MainView.ToolSize);

            Graphics graphics = Graphics.FromImage(View.pictureBox.Image);
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            graphics.DrawLine(pen, Beg, Curr);

            View.pictureBox.Invalidate();
            
        }

        public override void MouseUp(object sender, MouseEventArgs args)
        {
            MousePressed = false;
        }
    }
}
