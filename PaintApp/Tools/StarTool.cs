﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintApp.Tools
{
    class StarTool : CoreTool
    {
        Graphics _tempGraphics;
        Point _beg;
        Point _cur;
        Pen _pen;
        Image _image;
        public StarTool(PaintView view) : base(view)
        {
        }

        public override void MouseDown(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left)
            {
                MousePressed = true;
                _beg = args.Location;
                _image = View.pictureBox.Image;
                // tempGraphics = Graphics.FromImage(view.pictureBox.Image);
            }
        }

        public override void MouseMove(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                _cur = args.Location;
                this.Paint();
            }
        }
        public void Paint()
        {
            _pen = new Pen(MainView.Color, MainView.ToolSize);
            View.pictureBox.Image = (Image)_image.Clone();
            _tempGraphics = Graphics.FromImage(View.pictureBox.Image);
            DrawStar(_beg, _cur);

        }
        public void DrawStar(Point start,Point end)
        {
            int n = 5;               // число вершин
            double R = end.X, r = end.Y;   // радиусы
            double alpha = 0;        // поворот
            double x0 = start.X, y0 = start.Y; // центр

            PointF[] points = new PointF[2 * n + 1];
            double a = alpha, da = Math.PI / n, l;
            for (int k = 0; k < 2 * n + 1; k++)
            {
                l = k % 2 == 0 ? r : R;
                points[k] = new PointF((float)(x0 + l * Math.Cos(a)), (float)(y0 + l * Math.Sin(a)));
                a += da;
            }
            try
            {
                _tempGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                _tempGraphics.DrawLines(_pen, points);
            }
            catch (Exception)
            {

                //ignore
            }
           
        }

        public override void MouseUp(object sender, MouseEventArgs args)
        {
            if (MousePressed)
            {
                MousePressed = false;

                //tempGraphics.DrawLine(pen, beg, args.Location);
                View.pictureBox.Invalidate();
                DrawStar(_beg, _cur);
                _pen.Dispose();
                _tempGraphics.Dispose();
            }
        }
    }
}
